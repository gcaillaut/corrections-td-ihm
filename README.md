## Description

Correction des TD d'IHM pour les étudiants de première année du DUT informatique d'Orléans.

## Dépendances

Installer [R](https://www.r-project.org/) et le paquet `rmarkdown`.

```r
install.packages("rmarkdown")
```

Installer également [pandoc](https://pandoc.org/).

## Compilation

```r
library("rmarkdown")
render_site()
```